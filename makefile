# Project: VDI module
# Makefile

CPP = g++
LIBS = 
OBJ  = ./obj/VDICore.o ./obj/uuid.o ./obj/alloc.o ./obj/file.o ./obj/fs.o ./obj/rand.o ./obj/fileio.o ./obj/path.o ./obj/RTErrConvertFromErrno.o ./obj/filelock.o
OBJ_tstVDI = ./obj/tstVDI/tstVDI.o
OBJ_daemon = ./obj/daemon/vdi_daemon.o
LINKOBJ = ./obj/VDICore.o ./obj/uuid.o ./obj/alloc.o ./obj/file.o ./obj/fs.o ./obj/rand.o ./obj/fileio.o ./obj/path.o ./obj/RTErrConvertFromErrno.o ./obj/filelock.o
LINKOBJ_tstVDI = ./obj/tstVDI/tstVDI.o
LINKOBJ_daemon = ./obj/daemon/vdi_daemon.o
BIN  = tstVDI
DAEMON = vdi_daemon

make: $(OBJ) $(OBJ_tstVDI) $(OBJ_daemon)

./obj/tstVDI/tstVDI.o: ./src/tstVDI/tstVDI.cpp
	$(CPP) -c ./src/tstVDI/tstVDI.cpp -o ./obj/tstVDI/tstVDI.o

./obj/daemon/vdi_daemon.o: ./src/daemon/vdi_daemon.cpp
	$(CPP) -c ./src/daemon/vdi_daemon.cpp -o ./obj/daemon/vdi_daemon.o

./obj/VDICore.o: ./src/VDICore.cpp
	$(CPP) -c ./src/VDICore.cpp -o ./obj/VDICore.o

./obj/uuid.o: ./src/uuid.cpp
	$(CPP) -c ./src/uuid.cpp -o ./obj/uuid.o

./obj/alloc.o: ./src/alloc.cpp
	$(CPP) -c ./src/alloc.cpp -o ./obj/alloc.o

./obj/file.o: ./src/file.cpp
	$(CPP) -c ./src/file.cpp -o ./obj/file.o

./obj/fs.o: ./src/fs.cpp
	$(CPP) -c ./src/fs.cpp -o ./obj/fs.o

./obj/rand.o: ./src/rand.cpp
	$(CPP) -c ./src/rand.cpp -o ./obj/rand.o

./obj/fileio.o: ./src/fileio.cpp
	$(CPP) -c ./src/fileio.cpp -o ./obj/fileio.o

./obj/path.o: ./src/path.cpp
	$(CPP) -c ./src/path.cpp -o ./obj/path.o

./obj/RTErrConvertFromErrno.o: ./src/RTErrConvertFromErrno.cpp
	$(CPP) -c ./src/RTErrConvertFromErrno.cpp -o ./obj/RTErrConvertFromErrno.o

./obj/filelock.o: ./src/filelock.cpp
	$(CPP) -c ./src/filelock.cpp -o ./obj/filelock.o

clean:
	rm -f $(OBJ) $(OBJ_tstVDI) $(OBJ_daemon) ./src/*~ ./src/tstVDI/*~ ./src/daemon/*~ ./src/driver/*~ *~

bin:
	$(CPP) $(LIBS) $(LINKOBJ) $(LINKOBJ_tstVDI) -o $(BIN)

daemon:
	$(CPP) $(LIBS) $(LINKOBJ) $(LINKOBJ_daemon) -o $(DAEMON)
