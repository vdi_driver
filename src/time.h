#ifndef TIME_H
#define TIME_H

#include "cdefs.h"
#include <stdint.h>
#include <sys/time.h>

/** Time Specification.
 *
 * Use the inline RTTimeSpecGet/Set to operate on structure this so we
 * can easily change the representation if required later.
 *
 * The current representation is in nanoseconds relative to the unix epoch
 * (1970-01-01 00:00:00 UTC). This gives us an approximate span from
 * 1678 to 2262 without sacrifying the resolution offered by the various
 * host OSes (BSD & LINUX 1ns, NT 100ns).
 */
typedef struct RTTIMESPEC
{
    /** Nanoseconds since epoch.
     * The name is intentially too long to be comfortable to use because you should be
     * using inline helpers! */
    int64_t     i64NanosecondsRelativeToUnixEpoch;
} RTTIMESPEC;
/** Pointer to a time spec structure. */
typedef RTTIMESPEC *PRTTIMESPEC;
/** Pointer to a const time spec structure. */
typedef const RTTIMESPEC *PCRTTIMESPEC;


/**
 * Gets the time as microseconds relative to the unix epoch.
 *
 * @returns microseconds relative to unix epoch.
 * @param   pTime       The time spec to interpret.
 */
DECLINLINE(int64_t) RTTimeSpecGetMicro(PCRTTIMESPEC pTime)
{
    return pTime->i64NanosecondsRelativeToUnixEpoch / 1000;
}

/**
 * Gets the time as POSIX timeval.
 *
 * @returns pTime.
 * @param   pTime       The time spec to interpret.
 * @param   pTimeval    Where to store the time as POSIX timeval.
 */
DECLINLINE(struct timeval *) RTTimeSpecGetTimeval(PCRTTIMESPEC pTime, struct timeval *pTimeval)
{
    int64_t i64 = RTTimeSpecGetMicro(pTime);
    int32_t i32Micro = (int32_t)(i64 % 1000000);
    i64 /= 1000000;
    if (i32Micro < 0)
    {
        i32Micro += 1000000;
        i64++;
    }
    pTimeval->tv_sec = (time_t)i64;
    pTimeval->tv_usec = i32Micro;
    return pTimeval;
}

/**
 * Sets the time given by seconds relative to the unix epoch.
 *
 * @returns pTime.
 * @param   pTime       The time spec to modify.
 * @param   i64Seconds  The new time in seconds.
 */
DECLINLINE(PRTTIMESPEC) RTTimeSpecSetSeconds(PRTTIMESPEC pTime, int64_t i64Seconds)
{
    pTime->i64NanosecondsRelativeToUnixEpoch = i64Seconds * 1000000000;
    return pTime;
}

#endif
