/*
 * vdi.h - The driver for virtual disks of VirtualBox
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

//TODO: Remove? #include <linux/config.h>
#include <linux/module.h>
#include <linux/init.h>


#include <linux/blkdev.h>
#include <linux/cdev.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/genhd.h>
#include <linux/hdreg.h>
#include <linux/kernel.h>
#include <linux/types.h>

#include "vdi.h"

/*
 * Macros
 */
#define VDIS_BUF_SIZE 80

/*
 * Variables
 */
static int vdi_major  = VDI_MAJOR;
static int vdis_major = VDIS_MAJOR;


/*
 * Some disk parameters
 */
static int major    = VDI_MAJOR;


static vdi_dev vdi;
static struct request_queue *Queue;

// Used to prevent multiple access do vdis
static int is_vdis_open = 0;
static char vdis_buf[VDIS_BUF_SIZE];
static struct cdev vdis_cdev;
dev_t vdis_dev_t = MKDEV(VDIS_MAJOR, VDIS_MINOR);

//TODO: static char *vdis_buf


/*
 * Prototypes
 */

// Open the char device to communicate with daemon
static int vdis_open(struct inode *, struct file *);
static int vdis_release(struct inode *, struct file *);
static ssize_t vdis_read(struct file *, char *, size_t, loff_t *);
static ssize_t vdis_write(struct file *, const char *, size_t, loff_t *);


/*
 * Implementation
 */

static int vdis_open(struct inode *inode, struct file *file)
{
	if (is_vdis_open) {
		return -EBUSY;
	}
	is_vdis_open++;
	try_module_get(THIS_MODULE);

	return 0;
}


static int vdis_release(struct inode * inode, struct file * file)
{
	is_vdis_open--;
	return 0;
}


static ssize_t vdis_read(struct file * file, char * response, size_t length, loff_t * offset)
{
	int i;

	i = 0;
	while ( (vdis_buf[i] != '\0') && length ) {
		put_user(vdis_buf[i], response++);
		i++;
		length--;
	}
	vdis_buf[0] = '\0';
	return (i + 1);
}


static ssize_t vdis_write(struct file *file, const char *message, size_t length, loff_t *offset)
{
	return -EINVAL;
}


void vdi_request(request_queue_t *q)
{
	struct request *req;

    	while ( (req = elv_next_request(q)) != NULL ) {
        	if (!blk_fs_request(req)) {
			end_request(req, 0);
			continue;
		}
		
        	printk("vdi request: sec %lu (nr, %u)\n", req->nr_sectors,
               	       req->current_nr_sectors);
        	// TODO: send data to the daemon

		// Verify data direction
		if (rq_data_dir(req)) {
			// Write
			//sprintf(vdis_buf, "write %lu %lu\n", req->sector, req->nr_sectors);
		} else {
			// Read
			//sprintf(vdis_buf, "read %lu %lu\n", req->sector, req->nr_sectors);
		}

        	end_request(req, 1);
    }
}


int vdi_ioctl (struct inode *inode, struct file *filp, unsigned int cmd,
	       unsigned long arg)
{
	// Unknown command
	return -ENOTTY;
}

/*
 * Operations with the block driver
 */
static struct block_device_operations vdi_oper = {
	.owner = THIS_MODULE,
	.ioctl = vdi_ioctl
};


/*
 * Operations with the char driver
 */
struct file_operations vdis_oper = {
	.read    = vdis_read,
	.write   = vdis_write,
	.open    = vdis_open,
	.release = vdis_release
};


static int __init vdi_init(void)
{
	int err;
	/*
	 * Char device setup
	 */
	vdis_major = alloc_chrdev_region(&vdis_dev_t, 0, 1, "vdis");
	if (vdis_major < 0) {
		printk(KERN_WARNING "vdi: Unable to create the char device\n");
		return vdis_major;
	}
	vdis_major = MAJOR(vdis_dev_t);

	printk(KERN_WARNING "vdi: vdis major = %d\n", vdis_major);

	cdev_init(&vdis_cdev, &vdis_oper);
	vdis_cdev.owner = THIS_MODULE;
	vdis_cdev.ops   = &vdis_oper;
	err = cdev_add(&vdis_cdev, vdis_dev_t, 1);
	if (err) {
		printk(KERN_NOTICE "vdi: Error while adding char device\n");
	}


	/*
	 * Block device setup
	 */
	vdi.size = VDI_SECTORS * VDI_HARDSECT_SIZE;

	spin_lock_init(&vdi.lock);

    	Queue = blk_init_queue(vdi_request, &vdi.lock);
	if (Queue == NULL) {
		printk(KERN_WARNING "vdi: Unable to create request queue.\n");
		return -ENOMEM;
	}
	blk_queue_hardsect_size(Queue, VDI_HARDSECT_SIZE);


	/*
     	 * Register your major, and accept a dynamic number
     	 */
    	vdi_major = register_blkdev(vdi_major, "vdi");
    	if (vdi_major < 0) {
        	printk(KERN_WARNING "vdi: Unable to create block device major %d\n", vdi_major);
        	return vdi_major;
    	}

	// Create the gendisk structure
	vdi.gd = alloc_disk(16);
	if (!vdi.gd) {
		// TODO: Unregister it
		unregister_blkdev(major, "vdi");
	}

	// Set gendisk fields
	vdi.gd->major        = vdi_major;
	vdi.gd->first_minor  = 0;
	vdi.gd->fops         = &vdi_oper;
	vdi.gd->private_data = &vdi;
	strcpy(vdi.gd->disk_name, "vdi0");
	set_capacity(vdi.gd, VDI_SECTORS*(VDI_HARDSECT_SIZE/KERNEL_SECTOR_SIZE));
	vdi.gd->queue = Queue;

	add_disk(vdi.gd);
	printk("vdi: init complete\n");
   	return 0; /* succeed */
}

static void __exit vdi_exit(void)
{
	cdev_del(&vdis_cdev);
	unregister_chrdev_region(vdis_dev_t, 1);
	del_gendisk(vdi.gd);
	put_disk(vdi.gd);
	unregister_blkdev(vdi_major, "vdi");
	blk_cleanup_queue(Queue);
	printk("vdi: exit complete\n");
}

MODULE_LICENSE("GPL");

module_init(vdi_init);
module_exit(vdi_exit); 
