/*
 * vdi.h - The driver for virtual disks of VirtualBox
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ___vdi_h
#define ___vdi_h

#define VDI_DEBUG

#define VDI_MAJOR          0
#define VDI_SIZE           20
#define VDI_BLKSIZE        512
#define VDI_SECTORS        20
#define VDI_HARDSECT_SIZE  512

#define VDIS_MAJOR         0
#define VDIS_MINOR         0

#define KERNEL_SECTOR_SIZE 512

typedef struct vdi_dev {
	unsigned long size;		// Device size in bytes
	spinlock_t lock;		// Mutual exclusion
	//TODO: remove this?struct request_queue *queue;	// Device request queue
	struct gendisk *gd;		// gendisk structure
} vdi_dev;


asmlinkage int sys_subscribe_vdi_daemon(void);

#endif
