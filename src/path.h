/* $Id: path.h 23517 2007-08-07 17:07:59Z umoeller $ */
/** @file
 * innotek Portable Runtime - RTPath Internal header.
 */

/*
 * Copyright (C) 2006-2007 innotek GmbH
 *
 * This file is part of VirtualBox Open Source Edition (OSE), as
 * available from http://www.virtualbox.org. This file is free software;
 * you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation,
 * in version 2 as it comes in the "COPYING" file of the VirtualBox OSE
 * distribution. VirtualBox OSE is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY of any kind.
 */

#ifndef ___internal_path_h
#define ___internal_path_h

#include "cdefs.h"
#include "types.h"
#include "const_defines.h"

/** @name RTPathRename, RTDirRename & RTFileRename flags.
 * @{ */
/** This will replace attempt any target which isn't a directory. */
#define RTPATHRENAME_FLAGS_REPLACE      RT_BIT(0)
/** @} */

/** @def RTPATH_IS_SLASH
 * Checks if a character is a slash.
 *
 * @returns true if it's a slash and false if not.
 * @returns @param      ch      Char to check.
 */
#if defined(RT_OS_OS2) || defined(RT_OS_WINDOWS)
# define RTPATH_IS_SLASH(ch)    ( (ch) == '\\' || (ch) == '/' )
#else
# define RTPATH_IS_SLASH(ch)    ( (ch) == '/' )
#endif

/** @def RTPATH_SLASH
 * The prefered slash character.
 *
 * @remark IPRT will always accept unix slashes. So, normally you would
 *         never have to use this define.
 */
#if defined(RT_OS_OS2) || defined(RT_OS_WINDOWS)
# define RTPATH_SLASH       '\\'
#else
# define RTPATH_SLASH       '/'
#endif


int rtPathPosixRename(const char *pszSrc, const char *pszDst, unsigned fRename, RTFMODE fFileType);
int rtPathWin32MoveRename(const char *pszSrc, const char *pszDst, uint32_t fFlags, RTFMODE fFileType);


/**
 * Converts a path from IPRT to native representation.
 *
 * This may involve querying filesystems what codeset they
 * speak and so forth.
 *
 * @returns IPRT status code.
 * @param   ppszNativePath  Where to store the pointer to the native path.
 *                          Free by calling rtPathFreeHost(). NULL on failure.
 * @param   pszPath         The path to convert.
 * @remark  This function is not available on hosts using something else than byte seqences as names. (eg win32)
 */
int rtPathToNative(char **ppszNativePath, const char *pszPath);

/**
 * Converts a path from IPRT to native representation.
 *
 * This may involve querying filesystems what codeset they
 * speak and so forth.
 *
 * @returns IPRT status code.
 * @param   ppszNativePath  Where to store the pointer to the native path.
 *                          Free by calling rtPathFreeHost(). NULL on failure.
 * @param   pszPath         The path to convert.
 * @param   pszBasePath     What pszPath is relative to. If NULL the function behaves like rtPathToNative().
 * @remark  This function is not available on hosts using something else than byte seqences as names. (eg win32)
 */
int rtPathToNativeEx(char **ppszNativePath, const char *pszPath, const char *pszBasePath);

/**
 * Frees a native path returned by rtPathToNative() or rtPathToNativeEx().
 *
 * @param   pszNativePath   The host path to free. NULL allowed.
 * @remark  This function is not available on hosts using something else than byte seqences as names. (eg win32)
 */
void rtPathFreeNative(char *pszNativePath);

/**
 * Converts a path from the native to the IPRT representation.
 *
 * @returns IPRT status code.
 * @param   ppszPath        Where to store the pointer to the IPRT path.
 *                          Free by calling RTStrFree(). NULL on failure.
 * @param   pszNativePath   The native path to convert.
 * @remark  This function is not available on hosts using something else than byte seqences as names. (eg win32)
 */
int rtPathFromNative(char **ppszPath, const char *pszNativePath);

/**
 * Converts a path from the native to the IPRT representation.
 *
 * @returns IPRT status code.
 * @param   ppszPath        Where to store the pointer to the IPRT path.
 *                          Free by calling RTStrFree(). NULL on failure.
 * @param   pszNativePath   The native path to convert.
 * @param   pszBasePath     What pszHostPath is relative to - in IPRT representation.
 *                          If NULL the function behaves like rtPathFromNative().
 * @remark  This function is not available on hosts using something else than byte seqences as names. (eg win32)
 */
int rtPathFromNativeEx(char **ppszPath, const char *pszNativePath, const char *pszBasePath);

/**
 * Finds the filename in a path.
 *
 * @returns Pointer to filename within pszPath.
 * @returns NULL if no filename (i.e. empty string or ends with a slash).
 * @param   pszPath     Path to find filename in.
 */
char* RTPathFilename(const char *pszPath);

/**
 * Strips the filename from a path.
 *
 * @param   pszPath     Path which filename should be stripped.
 *                      If only filename in the string a '.' will be returned.
 */
void RTPathStripFilename(char *pszPath);

/**
 * Free string allocated by any of the non-UCS-2 string functions.
 *
 * @returns iprt status code.
 * @param   pszString      Pointer to buffer with string to free.
 *                         NULL is accepted.
 */
void  RTStrFree(char *pszString);

/**
 * Allocates tmp buffer, translates pszString from UTF8 to current codepage.
 *
 * @returns iprt status code.
 * @param   ppszString      Receives pointer of allocated native CP string.
 *                          The returned pointer must be freed using RTStrFree().
 * @param   pszString       UTF-8 string to convert.
 */
int  RTStrUtf8ToCurrentCP(char **ppszString, const char *pszString);

/**
 * Allocates tmp buffer, translates pszString from current codepage to UTF-8.
 *
 * @returns iprt status code.
 * @param   ppszString      Receives pointer of allocated UTF-8 string.
 *                          The returned pointer must be freed using RTStrFree().
 * @param   pszString       Native string to convert.
 */
int  RTStrCurrentCPToUtf8(char **ppszString, const char *pszString);


#endif

