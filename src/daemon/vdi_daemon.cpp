/*
 * vdi_daemon.c - Daemon of vdi driver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * To compile this file:
 *      g++ -o vdi_daemon vdi_daemon.cpp
 *
 * Substitute gcc with cc on some platforms.
 *
 * Daniel Borba / Tulio Magno
 * 27/6/2008
 *
 */
 
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <assert.h>
#include <signal.h>

#include "vdi_daemon.h"
#include "../VBoxHDD.h"
#include "../VDICore.h"
#include "../const_defines.h"

#define DAEMON_NAME "vdi_daemon"
#define PID_FILE "/var/run/vdi_daemon.pid"

//Global variables
int mRunDaemon = 0;
 
/**************************************************************************
    Function: Print Usage
 
    Description:
        Output the command-line options for this daemon.
 
    Params:
        @argc - Standard argument count
        @argv - Standard argument array
 
    Returns:
        returns void always
**************************************************************************/
void PrintUsage(int argc, char *argv[]) {
    if (argc >=1) {
        printf("Usage: %s -h -n\n", argv[0]);
        printf("  Options:\n");
        printf("      -n\tDon't fork off as a daemon.\n");
        printf("      -h\tShow this help screen\n");
        printf("\n");
    }
}
 
/**************************************************************************
    Function: signal_handler
 
    Description:
        This function handles select signals that the daemon may
        receive.  This gives the daemon a chance to properly shut
        down in emergency situations.  This function is installed
        as a signal handler in the 'main()' function.
 
    Params:
        @sig - The signal received
 
    Returns:
        returns void always
**************************************************************************/
void signal_handler(int sig) {
 
    switch(sig) {
        case SIGHUP:
            syslog(LOG_WARNING, "Received SIGHUP signal.");
            break;
        case SIGTERM:
            syslog(LOG_WARNING, "Received SIGTERM signal.");
	    mRunDaemon = 0;
            break;
        default:
            syslog(LOG_WARNING, "Unhandled signal (%d) %s", strsignal(sig));
            break;
    }
}
 
/**************************************************************************
    Function: main
 
    Description:
        The cpp standard 'main' entry point function.
 
    Params:
        @argc - count of command line arguments given on command line
        @argv - array of arguments given on command line
 
    Returns:
        returns integer which is passed back to the parent process
**************************************************************************/
int main(int argc, char *argv[]) {
 
#if defined(DEBUG)
    int daemonize = 0;
#else
    int daemonize = 1;
#endif

    // Setup signal handling before we start
    signal(SIGHUP, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGQUIT, signal_handler);
 
    int c;
    while( (c = getopt(argc, argv, "nh:|help")) != -1) {
        switch(c){
            case 'h':
                PrintUsage(argc, argv);
                exit(0);
                break;
            case 'n':
                daemonize = 0;
                break;
            default:
                PrintUsage(argc, argv);
                exit(0);
                break;
        }
    }
 
    syslog(LOG_INFO, "%s daemon starting up", DAEMON_NAME);
 
    // Setup syslog logging - see SETLOGMASK(3)
#if defined(DEBUG)
    setlogmask(LOG_UPTO(LOG_DEBUG));
    openlog(DAEMON_NAME, LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER);
#else
    setlogmask(LOG_UPTO(LOG_INFO));
    openlog(DAEMON_NAME, LOG_CONS, LOG_USER);
#endif

    /* Our process ID and Session ID */
    pid_t pid, sid;
 
    if (daemonize) {
        syslog(LOG_INFO, "starting the daemonizing process");
 
        /* Fork off the parent process */
        pid = fork();
        if (pid < 0) {
            exit(EXIT_FAILURE);
        }
        /* If we got a good PID, then
           we can exit the parent process. */
        if (pid > 0) {
            exit(EXIT_SUCCESS);
        }
 
        /* Change the file mode mask */
        umask(0);
 
        /* Create a new SID for the child process */
        sid = setsid();
        if (sid < 0) {
            /* Log the failure */
            exit(EXIT_FAILURE);
        }
 
        /* Change the current working directory */
        if ((chdir("/")) < 0) {
            /* Log the failure */
            exit(EXIT_FAILURE);
        }
 
        /* Close out the standard file descriptors */
        //TODO: close(STDIN_FILENO);
        //TODO: close(STDOUT_FILENO);
        //TODO: close(STDERR_FILENO);
    }
    //****************************************************
    // Daemon task
    //****************************************************
	//VDI Image File
	char vVdiImageFile [255] = "tstVdiBase.vdi";

	//VDI Request
	vdi_request* req = NULL;

	mRunDaemon = 1;

	while (mRunDaemon)
	{
		//Espera uma chamada do sistema que voltara um vdi_request (Eh necessario que se permita ao daemon escrever e ler no buffer da requisicao)
		if (req != NULL)
		{
			PVDIDISK vVdi = VDIDiskCreate();
			int rc = VDIDiskOpenImage(vVdi, vVdiImageFile, VDI_OPEN_FLAGS_NORMAL);

			if (VBOX_FAILURE(rc))
			{
				syslog(LOG_WARNING, "%s daemon: Can't open VDI image %s", DAEMON_NAME, vVdiImageFile);
			}
			else
			{
				unsigned long vOffsetStart = VDI_GEOMETRY_SECTOR_SIZE*req->sector;
				unsigned long vSize = VDI_GEOMETRY_SECTOR_SIZE*req->nr_sectors;

				switch(req->cmd) 
				{
					case VDI_READ:
						rc = VDIDiskRead(vVdi, vOffsetStart, req->buffer, vSize);

						if (VBOX_FAILURE(rc))
						{
							syslog(LOG_WARNING, "%s daemon: Can't read from VDI image %s", DAEMON_NAME, vVdiImageFile);
						}

						break;
					case VDI_WRITE:
						rc = VDIDiskWrite(vVdi, vOffsetStart, req->buffer, vSize);

						if (VBOX_FAILURE(rc))
						{
							syslog(LOG_WARNING, "%s daemon: Can't write in VDI image %s", DAEMON_NAME, vVdiImageFile);
						}

						break;
				}
			}

			VDIDiskCloseAllImages(vVdi);
		}
	}

 
    syslog(LOG_INFO, "%s daemon exiting", DAEMON_NAME);
 
    //****************************************************
    // TODO: Free any allocated resources before exiting
    //****************************************************
 
    exit(0);
}
