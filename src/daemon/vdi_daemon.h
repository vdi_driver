/*
 * vdi_daemon.h - Implementation of a daemon to access virtual disks of VirtualBox
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VDI_DAEMON_H
#define VDI_DAEMON_H

const int VDI_READ = 0;
const int VDI_WRITE = 1;

typedef struct vdi_request_struct
{
	int cmd;
	char* buffer;
	unsigned long sector;
	unsigned long nr_sectors;
} vdi_request;


#endif
