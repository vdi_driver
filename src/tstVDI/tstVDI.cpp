/** @file
 *
 * Simple VBox HDD container test utility.
 */

/*
 * Copyright (C) 2006-2007 innotek GmbH
 *
 * This file is part of VirtualBox Open Source Edition (OSE), as
 * available from http://www.virtualbox.org. This file is free software;
 * you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation,
 * in version 2 as it comes in the "COPYING" file of the VirtualBox OSE
 * distribution. VirtualBox OSE is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY of any kind.
 */

#include "../VBoxHDD.h"
#include "../file.h"
#include "../alloc.h"
#include <cstdio>
#include <cstring>


int dotest(const char *pszBaseFilename, const char *pszDiffFilename)
{
    PVDIDISK pVdi = VDIDiskCreate();

#define CHECK(str) \
    do \
    { \
        printf("%s rc=%i\n", str, rc); \
        if (VBOX_FAILURE(rc)) \
        { \
            VDIDiskCloseAllImages(pVdi); \
            return rc; \
        } \
    } while (0)


    int rc = VDIDiskOpenImage(pVdi, pszBaseFilename, VDI_OPEN_FLAGS_NORMAL);
    printf("openImage() rc=%i\n", rc);
    if (VBOX_FAILURE(rc))
    {
        rc = VDICreateBaseImage(pszBaseFilename, VDI_IMAGE_TYPE_NORMAL,
#ifdef _MSC_VER
                                (1000 * 1024 * 1024UI64),
#else
                                (1000 * 1024 * 1024ULL),
#endif
                                "Test image", NULL, NULL);
        CHECK("createImage()");

        rc = VDIDiskOpenImage(pVdi, pszBaseFilename, VDI_OPEN_FLAGS_NORMAL);
        CHECK("openImage()");
    }

    void *pvBuf = RTMemAlloc(1*1124*1024);

    memset(pvBuf, 'V', 1*1124*1024);
    rc = VDIDiskWrite(pVdi, 20*1024*1024 + 594040, pvBuf, 1024*1024);
    CHECK("write()");

    memset(pvBuf, 'D', 1*1124*1024);
    rc = VDIDiskWrite(pVdi, 30*1024*1024 + 594040, pvBuf, 1024*1024);
    CHECK("write()");

    memset(pvBuf, 'I', 1*1124*1024);
    rc = VDIDiskWrite(pVdi, 30*1024*1024 + 594040, pvBuf, 1024*1024);
    CHECK("write()");

    char buf[30] = "hello world";
    rc = VDIDiskWrite(pVdi, 0x0, buf, 30); // Reparem que aqui ele salva no endereço 0x00401200
    CHECK("write()");


    for (int i = 0; i < 20; i++) buf[i] = '\0';
    VDIDiskRead(pVdi, 0x0, buf, 11);

    printf("%s\n", buf);

    VDIDiskCloseAllImages(pVdi);
#undef CHECK
    return rc;
}


int main()
{
    RTFileDelete("tstVdiBase.vdi");
    RTFileDelete("tstVdiDiff.vdi");

    int rc = dotest("tstVdiBase.vdi", "tstVdiDiff.vdi");
    if (!rc)
        printf("tstVDI: SUCCESS\n");
    else
        printf("tstVDI: FAILURE\n");

//    RTFileDelete("tstVdiBase.vdi");
//    RTFileDelete("tstVdiDiff.vdi");
    return !!rc;
}
