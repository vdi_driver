#ifndef RTErrConvertFromErrno_H
#define RTErrConvertFromErrno_H

/**
 * Converts errno to iprt status code.
 *
 * @returns iprt status code.
 * @param   uNativeCode    errno code.
 */
int  RTErrConvertFromErrno(unsigned uNativeCode);

#endif
