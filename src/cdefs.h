#ifndef CDEFS_H
#define CDEFS_H

#include "config.h"

/** Size Constants
 * (Of course, these are binary computer terms, not SI.)
 * @{
 */
/** 1 K (Kilo)                     (1 024). */
#define _1K                     0x00000400
/** 4 K (Kilo)                     (4 096). */
#define _4K                     0x00001000
/** 32 K (Kilo)                   (32 678). */
#define _32K                    0x00008000
/** 64 K (Kilo)                   (65 536). */
#define _64K                    0x00010000
/** 128 K (Kilo)                 (131 072). */
#define _128K                   0x00020000
/** 256 K (Kilo)                 (262 144). */
#define _256K                   0x00040000
/** 512 K (Kilo)                 (524 288). */
#define _512K                   0x00080000
/** 1 M (Mega)                 (1 048 576). */
#define _1M                     0x00100000
/** 2 M (Mega)                 (2 097 152). */
#define _2M                     0x00200000
/** 4 M (Mega)                 (4 194 304). */
#define _4M                     0x00400000
/** 1 G (Giga)             (1 073 741 824). */
#define _1G                     0x40000000
/** 2 G (Giga)             (2 147 483 648). (32-bit) */
#define _2G32                   0x80000000U
/** 2 G (Giga)             (2 147 483 648). (64-bit) */
#define _2G             0x0000000080000000LL
/** 4 G (Giga)             (4 294 967 296). */
#define _4G             0x0000000100000000LL
/** 1 T (Tera)         (1 099 511 627 776). */
#define _1T             0x0000010000000000LL
/** 1 P (Peta)     (1 125 899 906 842 624). */
#define _1P             0x0004000000000000LL
/** 1 E (Exa)  (1 152 921 504 606 846 976). */
#define _1E             0x1000000000000000LL
/** 2 E (Exa)  (2 305 843 009 213 693 952). */
#define _2E             0x2000000000000000ULL
/** @} */

/** @def DECLINLINE
 * How to declare a function as inline.
 * @param   type    The return type of the function declaration.
 */
#ifdef __GNUC__
# define DECLINLINE(type) static inline type
#elif defined(__cplusplus)
# define DECLINLINE(type) inline type
#elif defined(_MSC_VER)
# define DECLINLINE(type) _inline type
#elif defined(__IBMC__)
# define DECLINLINE(type) _Inline type
#else
# define DECLINLINE(type) inline type
#endif

/** @def DECLR3CALLBACKMEMBER
 * How to declare an call back function pointer member - R3 Ptr.
 * @param   type    The return type of the function declaration.
 * @param   name    The name of the struct/union/class member.
 * @param   args    The argument list enclosed in parentheses.
 */
#ifdef IN_RING3
# define DECLR3CALLBACKMEMBER(type, name, args)  type (RTCALL * name) args
#else
# define DECLR3CALLBACKMEMBER(type, name, args)  RTR3PTR name
#endif

/** @def RT_LIKELY
 * Give the compiler a hint that an expression is very likely to hold true.
 *
 * Some compilers support explicit branch prediction so that the CPU backend
 * can hint the processor and also so that code blocks can be reordered such
 * that the predicted path sees a more linear flow, thus improving cache
 * behaviour, etc.
 *
 * IPRT provides the macros RT_LIKELY() and RT_UNLIKELY() as a way to utilize
 * this compiler feature when present.
 *
 * A few notes about the usage:
 *
 *      - Generally, use RT_UNLIKELY() with error condition checks (unless you
 *        have some _strong_ reason to do otherwise, in which case document it),
 *        and/or RT_LIKELY() with success condition checks, assuming you want
 *        to optimize for the success path.
 *
 *      - Other than that, if you don't know the likelihood of a test succeeding
 *        from empirical or other 'hard' evidence, don't make predictions unless
 *        you happen to be a Dirk Gently.
 *
 *      - These macros are meant to be used in places that get executed a lot. It
 *        is wasteful to make predictions in code that is executed seldomly (e.g.
 *        at subsystem initialization time) as the basic block reording that this
 *        affecs can often generate larger code.
 *
 *      - Note that RT_SUCCESS() and RT_FAILURE() already makes use of RT_LIKELY()
 *        and RT_UNLIKELY(). Should you wish for prediction free status checks,
 *        use the RT_SUCCESS_NP() and RT_FAILURE_NP() macros instead.
 *
 *
 * @returns the boolean result of the expression.
 * @param   expr        The expression that's very likely to be true.
 * @see RT_UNLIKELY
 */
/** @def RT_UNLIKELY
 * Give the compiler a hint that an expression is highly unlikely hold true.
 *
 * See the usage instructions give in the RT_LIKELY() docs.
 *
 * @returns the boolean result of the expression.
 * @param   expr        The expression that's very unlikely to be true.
 * @see RT_LIKELY
 */
#if defined(__GNUC__)
# if __GNUC__ >= 3
#  define RT_LIKELY(expr)       __builtin_expect(!!(expr), 1)
#  define RT_UNLIKELY(expr)     __builtin_expect(!!(expr), 0)
# else
#  define RT_LIKELY(expr)       (expr)
#  define RT_UNLIKELY(expr)     (expr)
# endif
#else
# define RT_LIKELY(expr)        (expr)
# define RT_UNLIKELY(expr)      (expr)
#endif

/** @def RT_LIKELY
 * Give the compiler a hint that an expression is very likely to hold true.
 *
 * Some compilers support explicit branch prediction so that the CPU backend
 * can hint the processor and also so that code blocks can be reordered such
 * that the predicted path sees a more linear flow, thus improving cache
 * behaviour, etc.
 *
 * IPRT provides the macros RT_LIKELY() and RT_UNLIKELY() as a way to utilize
 * this compiler feature when present.
 *
 * A few notes about the usage:
 *
 *      - Generally, use RT_UNLIKELY() with error condition checks (unless you
 *        have some _strong_ reason to do otherwise, in which case document it),
 *        and/or RT_LIKELY() with success condition checks, assuming you want
 *        to optimize for the success path.
 *
 *      - Other than that, if you don't know the likelihood of a test succeeding
 *        from empirical or other 'hard' evidence, don't make predictions unless
 *        you happen to be a Dirk Gently.
 *
 *      - These macros are meant to be used in places that get executed a lot. It
 *        is wasteful to make predictions in code that is executed seldomly (e.g.
 *        at subsystem initialization time) as the basic block reording that this
 *        affecs can often generate larger code.
 *
 *      - Note that RT_SUCCESS() and RT_FAILURE() already makes use of RT_LIKELY()
 *        and RT_UNLIKELY(). Should you wish for prediction free status checks,
 *        use the RT_SUCCESS_NP() and RT_FAILURE_NP() macros instead.
 *
 *
 * @returns the boolean result of the expression.
 * @param   expr        The expression that's very likely to be true.
 * @see RT_UNLIKELY
 */
/** @def RT_UNLIKELY
 * Give the compiler a hint that an expression is highly unlikely hold true.
 *
 * See the usage instructions give in the RT_LIKELY() docs.
 *
 * @returns the boolean result of the expression.
 * @param   expr        The expression that's very unlikely to be true.
 * @see RT_LIKELY
 */
#if defined(__GNUC__)
# if __GNUC__ >= 3
#  define RT_LIKELY(expr)       __builtin_expect(!!(expr), 1)
#  define RT_UNLIKELY(expr)     __builtin_expect(!!(expr), 0)
# else
#  define RT_LIKELY(expr)       (expr)
#  define RT_UNLIKELY(expr)     (expr)
# endif
#else
# define RT_LIKELY(expr)        (expr)
# define RT_UNLIKELY(expr)      (expr)
#endif

/** @def RT_BIT
 * Make a bitmask for one integer sized bit.
 * @param   bit     Bit number.
 */
#define RT_BIT(bit)                     (1U << (bit))

/** @def NOREF
 * Keeps the compiler from bitching about an unused parameters.
 */
#define NOREF(var)               (void)(var)

/** @def VALID_PTR
 * Pointer validation macro.
 * @param   ptr
 */
#if defined(RT_ARCH_AMD64)
# ifdef IN_RING3
#  if defined(RT_OS_DARWIN) /* first 4GB is reserved for legacy kernel. */
#   define VALID_PTR(ptr)   (   (uintptr_t)(ptr) >= _4G \
                             && !((uintptr_t)(ptr) & 0xffff800000000000ULL) )
#  else
#   define VALID_PTR(ptr)   (   (uintptr_t)(ptr) + 0x1000U >= 0x2000U \
                             && !((uintptr_t)(ptr) & 0xffff800000000000ULL) )
#  endif
# else /* !IN_RING3 */
#  define VALID_PTR(ptr)    (   (uintptr_t)(ptr) + 0x1000U >= 0x2000U \
                             && (   ((uintptr_t)(ptr) & 0xffff800000000000ULL) == 0xffff800000000000ULL \
                                 || ((uintptr_t)(ptr) & 0xffff800000000000ULL) == 0) )
# endif /* !IN_RING3 */
#elif defined(RT_ARCH_X86)
# define VALID_PTR(ptr)     ( (uintptr_t)(ptr) + 0x1000U >= 0x2000U )
#else
# error "Architecture identifier missing / not implemented."
#endif

#endif
