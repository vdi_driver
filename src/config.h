#ifndef CONFIG_H
#define CONFIG_H

#define R3_ARCH_BITS 32
#define ARCH_BITS 32
#define RT_INLINE_ASM_GNU_STYLE
//#define RT_ARCH_AMD64
#define RT_ARCH_X86
#define RT_OS_LINUX
//#define RT_OS_FREEBSD
//#define RT_OS_SOLARIS

#endif
