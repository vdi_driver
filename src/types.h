#ifndef TYPES_H
#define TYPES_H

#include "cdefs.h"
#include "config.h"
#include "time.h"
#include <stdint.h>

# define uintptr_t linux_uintptr_t
# include <linux/types.h>
# include <linux/stddef.h>
# undef uintptr_t

/** Unsigned integer which can contain a HC ring-3 pointer. */
#if R3_ARCH_BITS == 32
typedef uint32_t        RTR3UINTPTR;
#elif R3_ARCH_BITS == 64
typedef uint64_t        RTR3UINTPTR;
#else
#  error Unsupported R3_ARCH_BITS value.
#endif

/** Unsigned integer register in the current context. */
#if ARCH_BITS == 32
typedef uint32_t            RTCCUINTREG;
#elif ARCH_BITS == 64
typedef uint64_t            RTCCUINTREG;
#else
# error "Unsupported ARCH_BITS!"
#endif
/** Pointer to an unsigned integer register in the current context. */
typedef RTCCUINTREG        *PRTCCUINTREG;
/** Pointer to a const unsigned integer register in the current context. */
typedef const RTCCUINTREG  *PCRTCCUINTREG;

/** HC ring-3 pointer. */
#ifdef  IN_RING3
typedef void *          RTR3PTR;
#else
typedef RTR3UINTPTR     RTR3PTR;
#endif

/** File mode (see iprt/fs.h). */
typedef uint32_t        RTFMODE;
/** Pointer to file mode. */
typedef RTFMODE        *PRTFMODE;

/** User id. */
typedef uint32_t        RTUID;
/** Pointer to a user id. */
typedef RTUID          *PRTUID;

/** Group id. */
typedef uint32_t        RTGID;
/** Pointer to a group id. */
typedef RTGID          *PRTGID;

/** Device unix number. */
typedef uint32_t        RTDEV;
/** Pointer to a device unix number. */
typedef RTDEV          *PRTDEV;

/** i-node number. */
typedef uint64_t        RTINODE;
/** Pointer to a i-node number. */
typedef RTINODE        *PRTINODE;

/**
 * UUID data type.
 */
typedef union RTUUID
{
    /** 8-bit view. */
    uint8_t     au8[16];
    /** 16-bit view. */
    uint16_t    au16[8];
    /** 32-bit view. */
    uint32_t    au32[4];
    /** 64-bit view. */
    uint64_t    au64[2];
    /** The way the UUID is declared by the ext2 guys. */
    struct
    {
        uint32_t    u32TimeLow;
        uint16_t    u16TimeMid;
        uint16_t    u16TimeHiAndVersion;
        uint16_t    u16ClockSeq;
        uint8_t     au8Node[6];
    } Gen;
    /** @deprecated */
    unsigned char aUuid[16];
} RTUUID;
/** Pointer to UUID data. */
typedef RTUUID *PRTUUID;
/** Pointer to readonly UUID data. */
typedef const RTUUID *PCRTUUID;

/** Unsigned integer. */
typedef uint32_t        RTUINT;
/** File handle. */
typedef RTUINT                                      RTFILE;
/** Pointer to file handle. */
typedef RTFILE                                     *PRTFILE;
/** Nil file handle. */
#define NIL_RTFILE                                  (~(RTFILE)0)

/**
 * BIOS translation mode.
 */
typedef enum PDMBIOSTRANSLATION
{
    /** No translation. */
    PDMBIOSTRANSLATION_NONE = 1,
    /** LBA translation. */
    PDMBIOSTRANSLATION_LBA,
    /** Automatic select mode. */
    PDMBIOSTRANSLATION_AUTO
} PDMBIOSTRANSLATION;

/** Pointer to BIOS translation mode. */
typedef PDMBIOSTRANSLATION *PPDMBIOSTRANSLATION;

/** Pointer to a media interface. */
typedef struct PDMIMEDIA *PPDMIMEDIA;
/**
 * Media interface.
 * Makes up the fundation for PDMIBLOCK and PDMIBLOCKBIOS.
 */
typedef struct PDMIMEDIA
{
    /**
     * Read bits.
     *
     * @returns VBox status code.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @param   off             Offset to start reading from.
     * @param   pvBuf           Where to store the read bits.
     * @param   cbRead          Number of bytes to read.
     * @thread  Any thread.
     */
    DECLR3CALLBACKMEMBER(int, pfnRead,(PPDMIMEDIA pInterface, uint64_t off, void *pvBuf, size_t cbRead));

    /**
     * Write bits.
     *
     * @returns VBox status code.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @param   off             Offset to start writing at.
     * @param   pvBuf           Where to store the write bits.
     * @param   cbWrite         Number of bytes to write.
     * @thread  Any thread.
     */
    DECLR3CALLBACKMEMBER(int, pfnWrite,(PPDMIMEDIA pInterface, uint64_t off, const void *pvBuf, size_t cbWrite));

    /**
     * Make sure that the bits written are actually on the storage medium.
     *
     * @returns VBox status code.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @thread  Any thread.
     */
    DECLR3CALLBACKMEMBER(int, pfnFlush,(PPDMIMEDIA pInterface));

    /**
     * Get the media size in bytes.
     *
     * @returns Media size in bytes.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @thread  Any thread.
     */
    DECLR3CALLBACKMEMBER(uint64_t, pfnGetSize,(PPDMIMEDIA pInterface));

    /**
     * Check if the media is readonly or not.
     *
     * @returns true if readonly.
     * @returns false if read/write.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @thread  Any thread.
     */
    DECLR3CALLBACKMEMBER(bool, pfnIsReadOnly,(PPDMIMEDIA pInterface));

    /**
     * Get stored media geometry - BIOS property.
     * This is an optional feature of a media.
     *
     * @returns VBox status code.
     * @returns VERR_NOT_IMPLEMENTED if the media doesn't support storing the geometry.
     * @returns VERR_PDM_GEOMETRY_NOT_SET if the geometry hasn't been set using pfnBiosSetGeometry() yet.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @param   pcCylinders     Number of cylinders.
     * @param   pcHeads         Number of heads.
     * @param   pcSectors       Number of sectors. This number is 1-based.
     * @remark  This have no influence on the read/write operations.
     * @thread  Any thread.
     */
    DECLR3CALLBACKMEMBER(int, pfnBiosGetGeometry,(PPDMIMEDIA pInterface, uint32_t *pcCylinders, uint32_t *pcHeads, uint32_t *pcSectors));

    /**
     * Store the media geometry - BIOS property.
     * This is an optional feature of a media.
     *
     * @returns VBox status code.
     * @returns VERR_NOT_IMPLEMENTED if the media doesn't support storing the geometry.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @param   cCylinders      Number of cylinders.
     * @param   cHeads          Number of heads.
     * @param   cSectors        Number of sectors. This number is 1-based.
     * @remark  This have no influence on the read/write operations.
     * @thread  The emulation thread.
     */
    DECLR3CALLBACKMEMBER(int, pfnBiosSetGeometry,(PPDMIMEDIA pInterface, uint32_t cCylinders, uint32_t cHeads, uint32_t cSectors));

    /**
     * Get stored geometry translation mode - BIOS property.
     * This is an optional feature of a media.
     *
     * @returns VBox status code.
     * @returns VERR_NOT_IMPLEMENTED if the media doesn't support storing the geometry translation mode.
     * @returns VERR_PDM_TRANSLATION_NOT_SET if the translation hasn't been set using pfnBiosSetTranslation() yet.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @param   penmTranslation Where to store the translation type.
     * @remark  This have no influence on the read/write operations.
     * @thread  Any thread.
     */
    DECLR3CALLBACKMEMBER(int, pfnBiosGetTranslation,(PPDMIMEDIA pInterface, PPDMBIOSTRANSLATION penmTranslation));

    /**
     * Store media geometry - BIOS property.
     * This is an optional feature of a media.
     *
     * @returns VBox status code.
     * @returns VERR_NOT_IMPLEMENTED if the media doesn't support storing the geometry.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @param   enmTranslation  The translation type.
     * @remark  This have no influence on the read/write operations.
     * @thread  The emulation thread.
     */
    DECLR3CALLBACKMEMBER(int, pfnBiosSetTranslation,(PPDMIMEDIA pInterface, PDMBIOSTRANSLATION enmTranslation));

    /**
     * Gets the UUID of the media drive.
     *
     * @returns VBox status code.
     * @param   pInterface      Pointer to the interface structure containing the called function pointer.
     * @param   pUuid           Where to store the UUID on success.
     * @thread  Any thread.
     */
    DECLR3CALLBACKMEMBER(int, pfnGetUuid,(PPDMIMEDIA pInterface, PRTUUID pUuid));

} PDMIMEDIA;

/** Pointer to a PDM Driver Instance. */
typedef struct PDMDRVINS *PPDMDRVINS;
/** Pointer to a pointer to a PDM Driver Instance. */
typedef PPDMDRVINS *PPPDMDRVINS;

/** A file offset / size (off_t). */
typedef int64_t         RTFOFF;
/** Pointer to a file offset / size. */
typedef RTFOFF         *PRTFOFF;

/** File mode (see iprt/fs.h). */
typedef uint32_t        RTFMODE;
/** Pointer to file mode. */
typedef RTFMODE        *PRTFMODE;

/**
 * Progress callback.
 * This will report the completion percentage of an operation.
 *
 * @returns VINF_SUCCESS.
 * @returns Error code to cancel the operation with.
 * @param   pVM         The VM handle.
 * @param   uPercent    Completetion precentage (0-100).
 * @param   pvUser      User specified argument.
 */
typedef int FNVMPROGRESS(void* pVM, unsigned uPercent, void *pvUser);
/** Pointer to a FNVMPROGRESS function. */
typedef FNVMPROGRESS *PFNVMPROGRESS;

/**
 * Generic process callback.
 *
 * @returns VBox status code. Failure will cancel the operation.
 * @param   uPercentage     The percentage of the operation which has been completed.
 * @param   pvUser          The user specified argument.
 */
typedef int FNRTPROGRESS(unsigned uPrecentage, void *pvUser);
/** Pointer to a generic progress callback function, FNRTPROCESS(). */
typedef FNRTPROGRESS *PFNRTPROGRESS;

/**
 * 64-bit unsigned interger union.
 */
typedef union RTUINT64U
{
    /** Natural view. */
    uint64_t    u;
    /** Hi/Low view. */
    struct
    {
        uint32_t    Lo;
        uint32_t    Hi;
    } s;
    /** Double-Word view. */
    struct
    {
        uint32_t    dw0;
        uint32_t    dw1;
    } DWords;
    /** Word view. */
    struct
    {
        uint16_t    w0;
        uint16_t    w1;
        uint16_t    w2;
        uint16_t    w3;
    } Words;

    /** 64-bit view. */
    uint64_t    au64[1];
    /** 32-bit view. */
    uint32_t    au32[2];
    /** 16-bit view. */
    uint16_t    au16[4];
    /** 8-bit view. */
    uint8_t     au8[8];
} RTUINT64U;
/** Pointer to a 64-bit unsigned interger union. */
typedef RTUINT64U *PRTUINT64U;
/** Pointer to a const 64-bit unsigned interger union. */
typedef const RTUINT64U *PCRTUINT64U;

/**
 * UTF-16 character.
 * @remark  wchar_t is not usable since it's compiler defined.
 * @remark  When we use the term character we're not talking about unicode code point, but
 *          the basic unit of the string encoding. Thus cuc - count of unicode chars - means
 *          count of RTUTF16. And cch means count of the typedef 'char', which is assumed
 *          to be an octet.
 */
typedef uint16_t        RTUTF16;
/** Pointer to a UTF-16 character. */
typedef RTUTF16        *PRTUTF16;
/** Pointer to a const UTF-16 character. */
typedef const RTUTF16  *PCRTUTF16;

/**
 * UCS-2 character.
 * @remark wchar_t is not usable since it's compiler defined.
 * @deprecated  Use RTUTF16!
 */
typedef RTUTF16         RTUCS2;
/** Pointer to UCS-2 character.
 * @deprecated  Use PRTUTF16!
 */
typedef PRTUTF16        PRTUCS2;
/** Pointer to const UCS-2 character.
 * @deprecated  Use PCRTUTF16!
 */
typedef PCRTUTF16       PCRTUCS2;

#endif
